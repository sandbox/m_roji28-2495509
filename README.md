role student can edit own thesis content which field_status not approved
role student can edit own thesis content whose field_status not approved

role student can view any thesis content which field_authors :self:
role student can view any thesis content whose field_authors :self:


# Idea 
This module has solved my problem:
I have a Content Type named Thesis. That content can has single author or
multi author. jika skripsi tersebut merupakan banyak pengarang, maka skripsi
tersebut hanya 

# Usage

Use syntax query with these structure:

role $rid[ whose $field_name $value[ (and|or) $...]] (can|cannot|can not|can't) $permission[ $extras]
which:
  - $rid = Role's Id, or Role's machine_name if module role_export exists.
  - $field_name = User's field machine name only for field type: taxonomy_term or
    entity_reference for taxonomy term.
  - $value = Taxonomy term's Id or Taxonomy term's machine name if module 
    taxonomy_machine_name exists.
  - $permission = the access
  - $extras = Extra conditions for permission. 

# Example

## simple example
role member can create article content
role member can create page content

## complex for scholl management
role student can view own thesis
role student can view any thesis content whose field_authors :self:
role student can't view any thesis content
role student can edit own thesis content which field_status not approved
role student can't edit own thesis content


  
  
# This query was auto populate form permission page
# for your first runtime of this module.

role 2 whose field_gender 0 can edit own skripsi content 

role pustakawan whose field_gender 0 can edit own skripsi content 




role mahasiswa whose field_author :self: can view anyskripsi content
role mahasiswa whose field_author :self: can view anyskripsi content









ada simple query dan ada complex query.

simple query akan otomatis di synkronkan dengan permssion page.




###
# Welcome to textarea of content access query
#
# Every line which started with pound sign (#) or every word after pound sign
# means comment and will be ignore for query.
#
# The structure of query like this:
# [Subject][Access][Permission]

#
# A simple example:
# role 1 can create article content
# "role 1" is a Subject
# "can" is an Access
# "create article content" is a Permission
#
# If you has module role_export exists, you can use role's machine name
#
# Example:
# role lecturer can create article content
#
# For Subject, you can specify with user and its field's of taxonomy term
# or entity reference that target to taxonomy term
#
# Example:
# user field_gender 1 can create article content
# role lecturer whose field_gender 43 can create article content
#
# If you has module taxonomy_machine_name exists, you can use
# taxonomy_term's machine name.
#
# user field_gender male can create article content
#
# For Subject, you can mix role and user even use everyone
#
# Example:
# role lecturer whose field_gender male can create article content
#
###