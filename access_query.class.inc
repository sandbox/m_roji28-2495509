<?php

class AccessQuery {

  public static $regex = '^role\s+(?<role>[[:alnum:]]+)\s+can\s+(?<permission>.*)$';

  public static function buildRawQueriesFromPermissionPage() {
    $queries = self::buildQueriesFromPermissionPage(true);
    return self::buildRawQueriesFromQueries($queries, true);    
  }
  
  public static function buildRawQueriesFromQueries($queries, $group_by_module = false, $options = array()) {
    $options += array(
      'prefix' => '',
      'suffix' => '',
    );
    $prefix = empty($options['prefix']) ? '' : $options['prefix'] . ' ';
    $suffix = empty($options['suffix']) ? '' : ' ' . $options['suffix'];
    $raw_queries = '';
    if ($group_by_module) {
      if (!empty($queries)) {
        foreach ($queries as $module => $list) {
          $raw_queries .= '# ' . $prefix . $module . ' module' . $suffix . "\r\n";
          $raw_queries .= '' . implode("\r\n", $list);
          $raw_queries .= "\r\n";
          $raw_queries .= "\r\n";
        }
      }
    }
    return $raw_queries;
    
  }
  

  // mengembalikan array satu dimensi 
  // dimana element array merupakan raw query
  // atau jika argumnet $group_by_module = true
  // maka akan mengembalikan array dua dimensi, 
  // dimana dimensi pertama adalah assosiative array 
  // dengan index berupa nama module dan 
  // dimensi kedua merupakan array dimana element
  public static function buildQueriesFromPermissionPage($group_by_module = false) {
    $role_names = user_roles();
    $role_permissions = user_role_permissions($role_names);
    $role_permissions = array_filter($role_permissions);
    // $debugname = 'role_permissions'; dpm($$debugname, '$' . $debugname);
    $list_permissions = user_permission_get_modules();
    // $debugname = 'list_permissions'; dpm($$debugname, '$' . $debugname);
    $storage = array();
    if ($group_by_module) {
      foreach ($role_permissions as $role => $permissions) {
        foreach (array_keys($permissions) as $permission) {
          $storage[$list_permissions[$permission]][] = 'role ' . $role . ' can ' . $permission;
        }
      }
      ksort($storage);
    }
    else {
      foreach ($role_permissions as $role => $permissions) {
        foreach (array_keys($permissions) as $permission) {
          $storage[] = 'role ' . $role . ' can ' . $permission;
        }
      }
    }
    return $storage;
  }

  public static function buildQueriesFromRawQueries($raw_queries = null) {
    if (is_null($raw_queries)) {
      $raw_queries = variable_get('access_query_raw_queries', '');
    }   
    $lines = preg_split('[\r\n]', $raw_queries);
    $queries_custom = array();
    foreach ($lines as $i => $line) {
      $clean = self::cleanRawQuery($line);
      if (!empty($clean)) {
        $queries_custom[] = $clean;
      }
    }
    return $queries_custom;
  }
  
  /**
   * Fungsi untuk memvalidasi inputan user tentang query.
   */
  public static function validateRawQueries($string) {
    if (empty($string)) {
      return;
    }
    // Split berdasarkan \r dan atau \n.
    $lines = preg_split('[\r\n]', $string);
    foreach ($lines as $i => $line) {
      if ($message = self::validateRawQuery($line)) {
        $message = t('Error at line !order: %line. !message', array('!order' => $i + 1, '%line' => $line, '!message' => $message));
        return $message;
      }
    }
  }

  public static function validateRawQuery($string, $return = 'message error') {
    $raw_query = self::cleanRawQuery($string);
    if (empty($raw_query)) {
      return;
    }

    // Let's validate.
    try {
      // We don't need double raw_query
      static $storage = array();
      $raw_query_low = strtolower($raw_query);
      if (!in_array($raw_query_low, $storage)) {
        $storage[] = $raw_query_low;
      }
      else {
        $message = t('Duplicate query.');
        throw new Exception($message);
      }
      // Start parsing query per line.

      $query = self::extractRawQuery($raw_query, true);
      // For Developing.
      // $debugname = 'query'; dpm($$debugname, '$' . $debugname);

      // Validate each component of query.
      // Variable $query is an array should have index:
      // role and permissions.
      if (empty($query)) {
        $message = t('Unknown format.');
        throw new Exception($message);
      }
      // Validate role.
      if ($message = self::validateRole($query['role'])) {
        throw new Exception($message);
      }
      // Validate permissions.
      if ($message = self::validatePermission($query['permission'])) {
        throw new Exception($message);
      }

    }
    catch (Exception $e) {
      return $e->getMessage();
    }
  }

  // Membersihkan raw query dari komentar, dan
  // sekalian trim dari whitespace.
  protected static function cleanRawQuery($string) {
    $string = trim($string);
    if (empty($string)) {
      return $string;
    }
    static $storage = array();
    // $debugname = 'storage'; dpm($$debugname, '$' . $debugname);
    if (!array_key_exists($string, $storage)) {
      $clean_string = $string;
      // Clean raw_query from comment.
      $comment_sign_position = strpos($clean_string, '#');
      if ($comment_sign_position === 0) {
        $clean_string = '';
      }
      elseif (is_int($comment_sign_position)) {
        $clean_string = rtrim(substr($clean_string, 0, $comment_sign_position));
      }
      $storage[$string] = $clean_string;
    }
    return $storage[$string];
  }

  // parsing raw query dan dapatkan array query.
  protected static function extractRawQuery($raw_query, $is_clean = false) {
    if (!$is_clean) {
      $raw_query = self::cleanRawQuery($raw_query);
      if (empty($raw_query)) {
        return;
      }
    }
    static $storage = array();
    if (!array_key_exists($raw_query, $storage)) {
      $regex = self::$regex;
      preg_match('/' . $regex . '/i', $raw_query, $query);
      $storage[$raw_query] = $query;
    }
    return $storage[$raw_query];
  }

  protected static function validateRole($check) {
    static $storage = array();
    if (!array_key_exists($check, $storage)) {
      // Set first as no message.
      $storage[$check] = NULL;
      try {
        if (is_numeric($check)) {
          $list = user_roles();
          if (!isset($list[$check])) {
            $message = t('Unknown role ID: @rid.', array('@rid' => $check));
            throw new Exception($message);
          }
        }
        elseif (module_exists('role_export')) {
          $role_load =  db_select('role', 'r')
            ->fields('r')
            ->condition('machine_name', $check)
            ->execute()
            ->fetchObject();
          if (!$role_load) {
            $message = t('Unknown role machine_name: %check.', array('%check' => $check));
            throw new Exception($message);
          }
        }
        else {
          $message = t('Unknown Role: %check, you must set valid role ID or role machine_name (which provided by <a href=!url>Role Export</a> module).', array('%check' => $check, '!url' => 'http://drupal.org/project/role_export'));
          throw new Exception($message);
        }
      }
      catch (Exception $e) {
        $storage[$check] = $e->getMessage();
      }
    }
    return $storage[$check];
  }

  protected static function validatePermission($check) {
    static $storage = array();
    if (!array_key_exists($check, $storage)) {
      // Set first as no message.
      $storage[$check] = NULL;
      try {
        $list_permissions = user_permission_get_modules();
        if (!array_key_exists($check, $list_permissions)) {
          $message = t('Unknown Permission: %permission.', array('%permission' => $check));
          throw new Exception($message);
        }
      }
      catch (Exception $e) {
        $storage[$check] = $e->getMessage();
      }
    }
    return $storage[$check];
  }

  public static function submit($raw_queries) {

    $queries_custom = self::buildQueriesFromRawQueries($raw_queries);
    
    $queries_system = self::buildQueriesFromPermissionPage();

    // $debugname = 'queries_system'; dpm($$debugname, '$' . $debugname);
    // Check changes.
    $queries_grant = array_diff($queries_custom, $queries_system);
    // $queries_grant = $queries_custom;
    $queries_revoke = array_diff($queries_system, $queries_custom);

    
    // Grant.
    $debugname = 'queries_grant'; dpm($$debugname, '$' . $debugname);
    if (!empty($queries_grant)) {
      $group = array();
      foreach ($queries_grant as $raw_query) {
        $query = self::extractRawQuery($raw_query, true);
        $group[$query['role']][] = $query['permission'];
      }
      foreach ($group as $role => $permission) {
        // user_role_grant_permissions($role, $permission);
      }
    }

    // Revoke.
    $debugname = 'queries_revoke'; dpm($$debugname, '$' . $debugname);
    if (!empty($queries_revoke)) {
      $group = array();
      foreach ($queries_revoke as $raw_query) {
        $query = self::extractRawQuery($raw_query, true);
        $group[$query['role']][] = $query['permission'];
      }
      foreach ($group as $role => $permission) {
        // user_role_revoke_permissions($role, $permission);
      }
      // $debugname = 'group'; dpm($$debugname, '$' . $debugname);
    }
  }

  public static function synchronize() {
    // $raw_queries = variable_get('access_query_raw_queries');
    // $debugname = 'raw_queries'; dpm($$debugname, '$' . $debugname);
    $queries_system = self::buildQueriesFromPermissionPage();
    // $debugname = 'queries_system'; dpm($$debugname, '$' . $debugname);
    
    $queries_custom = self::buildQueriesFromRawQueries();
    $debugname = 'queries_custom'; dpm($$debugname, '$' . $debugname);
    
    $queries_added = array_diff($queries_system, $queries_custom);
    $debugname = 'queries_added'; dpm($$debugname, '$' . $debugname);
    
    $queries_remove = array_diff($queries_custom, $queries_system);
    $debugname = 'queries_remove'; dpm($$debugname, '$' . $debugname);
    
    $role_names = user_roles();
    $role_permissions = user_role_permissions($role_names);
    $role_permissions = array_filter($role_permissions);
    $debugname = 'role_permissions'; dpm($$debugname, '$' . $debugname);
    
    // Removed.
    $raw_queries = variable_get('access_query_raw_queries');
    $raw_queries_is_changed = FALSE;
    
    // $debugname = 'a'; dpm($$debugname, '$' . $debugname);
    if (!empty($queries_remove)) {
      foreach ($queries_remove as $raw_query) {
        $raw_queries = str_replace($raw_query, "# DELETE $raw_query", $raw_queries);
      }
      $raw_queries_is_changed = TRUE;      
    }
    
    
    
    // Added.
    if (!empty($queries_added)) {
      $storage = array();
      $list_permissions = user_permission_get_modules();
      foreach ($queries_added as $raw_query) {
        $query = self::extractRawQuery($raw_query, true);
        $module = $list_permissions[$query['permission']];
        // $debugname = 'module'; dpm($$debugname, '$' . $debugname);
        // $debugname = 'query'; dpm($$debugname, '$' . $debugname);
        $storage[$module][] = $raw_query;
      }
      // $debugname = 'storage'; dpm($$debugname, '$' . $debugname);
      $new_raw_queries = self::buildRawQueriesFromQueries($storage, true, ['suffix' => 'NEW']);
      $raw_queries = $new_raw_queries . $raw_queries;
      $raw_queries_is_changed = TRUE;  
    }
    
    if ($raw_queries_is_changed) {
      variable_set('access_query_raw_queries', $raw_queries);
    }
    // $debugname = 'raw_queries'; dpm($$debugname, '$' . $debugname);
    
    
  }
}
