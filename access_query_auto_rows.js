(function($) {
  Drupal.behaviors.contentAccessQuery = {
    attach: function (context, settings) {
      // console.log(settings);
      for (var key in settings.access_query_auto_rows) {
        var selector = settings.access_query_auto_rows[key];
        $(selector, context).once('caq', function() {
          $(this).keydown(function(event) {
            // For quick autoheigth
            if (event.keyCode == 13) {
              this.rows++;
            }
          }).keyup( function(event){
            // Recheck again.
            var row = this.value.split("\n").length;
            this.rows = row;
          });
        });
      }
    }
  }
})(jQuery);