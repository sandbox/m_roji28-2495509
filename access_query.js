(function($) {
  Drupal.behaviors.contentAccessQuery = {
    attach: function (context, settings) {
      $('#access-query-node-sources', context).once('caq', function() {
        // Drupal.contentAccessQueryAutoRows($(this).find('#query_above'));
        // Drupal.contentAccessQueryAutoRows($(this).find('#query_below'));
        // Drupal.contentAccessQueryAutoRows($(this).find('#query_custom'));
        $(this).find('#query_custom').keydown(function(event) {
          // For quick autoheigth
          if (event.keyCode == 13) {
            this.rows++;
          }
        }).keyup( function(event){
          // Recheck again.
          var row = this.value.split("\n").length;
          this.rows = row;
        });
      });
    }
  }

  // todo, delete this. ganti dengan auto row by php.
  Drupal.contentAccessQueryAutoRows = function (element) {
    var value = element.val();
    var rows = value.split("\n").length;
    element.attr('rows', rows);
  }

})(jQuery);