<?php

function access_query_ui($form, &$form_state) {
  $is_rebuild = variable_get('access_query_need_synchronize');
  if ($is_rebuild) {
    return access_query_sources_synchronize($form, $form_state);
  }
  $form['fake']['#markup'] = 'a';
  return $form;
}



function access_query_sources($form, &$form_state) {
  $is_rebuild = variable_get('access_query_need_synchronize');
  if ($is_rebuild) {
    return access_query_sources_synchronize($form, $form_state);
  }

  // $form['fake']['#markup'] = 'a';
  $default_value = variable_get('access_query_raw_queries','');
  if (empty($default_value)) {
    $default_value = AccessQuery::buildRawQueriesFromPermissionPage();
    variable_set('access_query_raw_queries', $default_value);
  }
  $form['access_query_raw_queries'] = array(
    '#type' => 'textarea',
    '#default_value' => $default_value,
    '#rows' => count(preg_split('[\r\n]', $default_value)),
    '#resizable' => FALSE,
    '' => '',
  );
  $form['#attached'] = array(
    'js' => array(
      drupal_get_path('module', 'access_query') . '/access_query_auto_rows.js',
      array(
        'type' => 'setting',
        'data' => array(
          'access_query_auto_rows' => array(
            'textarea[name=access_query_raw_queries]',
          ),
        ),
      ),
    ),
  );
  // To save anything in variable system.
  // $form['#submit'][] = 'system_settings_form_submit';

  // return $form;
  // return system_settings_form($form);
  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));
  $form['actions']['reset'] = array('#type' => 'submit', '#value' => t('Reset'));
  return $form;
}

function access_query_sources_validate(&$form, &$form_state) {

  // $val = $form_state['values'];
  // $debugname = 'val'; dpm($$debugname, '$' . $debugname);
  return;
  $message = AccessQuery::validateRawQueries($form_state['values']['access_query_raw_queries']);
  if (!empty($message)) {
    // Repair value of #rows with current value.
    $current_value = $form['access_query_raw_queries']['#value'];
    $form['access_query_raw_queries']['#rows'] = count(preg_split('[\r\n]', $current_value));
    // Set error.
    form_set_error('access_query_raw_queries', $message);
  }
}

function access_query_sources_submit(&$form, &$form_state) {
  $op = $form_state['values']['op'];
  switch ($op) {
      case t('Synchronize'):
        AccessQuery::synchronize();
        variable_set('access_query_need_synchronize', FALSE);        
        // 
        break;
  
      case '':
        // Do something.
        break;
  
      default:
        AccessQuery::submit($form_state['values']['access_query_raw_queries']);        
        variable_set('access_query_raw_queries', $form_state['values']['access_query_raw_queries']);        
        break;
  }
  
  // $val = $form_state['values'];
  // $debugname = 'val'; dpm($$debugname, '$' . $debugname);
}

function access_query_sources_synchronize($form, &$form_state) {
  $description = t('The changes in <a href=@url>permissions page</a> detected. We must synchronize first before continue.', array('@url' => url('admin/people/permissions')));
  $title = t('Require to synchronize');
  $path_cancel = 'admin/people';
  $yes = t('Synchronize');
  $no = t('Not now');
  $name = 'synchronize';
  return confirm_form($form, $title, $path_cancel, $description, $yes, $no, $name);
}

// [ ] Show label instead of machine name.
// If checked, query like this role 1 can create article content
// will be show like this Anonymous user can Article: Create new content

// You had change role access from Permissions Page.
// Click the update button to start synchronize.
// [Update]
// [Ignore and keep my existing queries.]

// [Insert Query]