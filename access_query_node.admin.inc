<?php

function access_query_node_sources($form, &$form_state) {
  
  // $role_permissions = user_role_permissions($role_names);
  
  // $a = user_permission_get_modules();
  // $debugname = 'a'; dpm($$debugname, '$' . $debugname);
  // $x = node_permissions_get_configured_types();
  // $debugname = 'x'; dpm($$debugname, '$' . $debugname);
  
  // $list = ;
  // $node_permission = implode('|', array_keys(access_query_node_node_permissions()));  
  // $debugname = 'node_permission'; dpm($$debugname, '$' . $debugname);
  // $a = 'role 1 whose field_gender can not can not create page content ';
  // $a = 'role 1 can not create page content';
  // $a = strtolower($a);
  
  // $regex = 'role\s(?<role>[[:alnum:]])(\swhose(?<spesific>.+))*\s(?<access>can|cannot|can not|can\'t)\s(?<permission>' . $node_permission . ')(\s(?<extras>.*))*';  
  // $b = preg_match('/' . $regex . "/", $a, $m);
  
  
  // $debugname = 'm'; dpm($$debugname, '$' . $debugname);
  // $role_names = user_roles();
  // $b = user_role_permissions($role_names);
  // $debugname = 'b'; dpm($$debugname, '$' . $debugname);
  
  // $role_names = user_roles();
  // $debugname = 'role_names'; dpm($$debugname, '$' . $debugname);
  
  // $c = user_role_permissions($role_names);
  // $debugname = 'c'; dpm($$debugname, '$' . $debugname);
  
  // $result = db_query("SELECT rid, permission FROM {role_permission} WHERE rid IN (:fetch)", array(':fetch' => $fetch));
  // $result = db_query("SELECT rid FROM {role_permission} WHERE permission = :permission", array(':permission' => 'access content'))->fetchAllAssoc('rid');
  // $debugname = 'result'; dpm($$debugname, '$' . $debugname);
  // foreach ($result as $row) {
    // $debugname = 'row'; dpm($$debugname, '$' . $debugname);
  // }
  $value = variable_get('access_query_node_custom','');
  
  // Jika Permission page (admin/people/permissions) telah disubmit, 
  // maka kita memerlukan cek ulang
  // query yang telah exists untuk memastikan bahwa 
  // query dari permissions page tersebut juga diakomodir dengan
  // cara diset secara otomatis pada query custom.
  $is_rebuild = variable_get('access_query_node_rebuild', TRUE);
  if ($is_rebuild) {
    $query_permissions = AccessQueryNode::buildQueriesFromPermissionPage();
    $debugname = 'query_permissions'; dpm($$debugname, '$' . $debugname);
    $query_custom = AccessQueryNode::buildQueriesFromString($value);
    $debugname = 'query_custom'; dpm($$debugname, '$' . $debugname);
    $query_permissions_filtered = array_diff($query_permissions, $query_custom);
    if (!empty($query_permissions_filtered)) {        
      // Add queries at bottom.
      $prefix = '# ' . t('Those settings are automatically loaded from permission page.');
      $value = $value . "\r\n" . $prefix . "\r\n" . implode("\r\n", $query_permissions_filtered) . "\r\n";
    }
    $form['#submit'][] = 'access_query_node_remove_rebuild_flag';
    // $debugname = 'query_permissions_filtered'; dpm($$debugname, '$' . $debugname);
    // $debugname = 'query'; dvm($$debugname, '$' . $debugname);
  }
  
  $form['access_query_node_above'] = array(
    // '#title' => 'Query Above.',
    // '#description' => t('Query from node_acces() always above from your custom. You can edit this from <a href="@permissions">Permissions page</a>', array('@permissions' => url('admin/people/permissions', array('fragment' => 'module-node')))),
    '#id' => 'query_above',
    '#type' => 'textarea',
    // '#rows' => '1',
    '#disabled' => TRUE,
    '#resizable' => FALSE,
    '#default_value' => access_query_node_above(),
    '#access' => FALSE,
  );
  
  
  $form['access_query_node_custom'] = array(
    '#id' => 'query_custom',
    '#type' => 'textarea',
    '#rows' => count(preg_split('[\r\n]', $value)),
    '#resizable' => FALSE,
    '#default_value' => $value,
    // '#default_value' => 'role 1 can create article content',
  );
  $form['access_query_node_below'] = array(
    // '#title' => 'Query Below.',
    // '#description' => t('Query from node_node_acces() always below from your custom. You can edit this from <a href="@permissions">Permissions page</a>', array('@permissions' => url('admin/people/permissions', array('fragment' => 'module-node')))),
    '#id' => 'query_below',
    '#type' => 'textarea',
    // '#rows' => '1',
    '#disabled' => TRUE,
    '#resizable' => FALSE,
    '#default_value' => 'role "administrator" CAN "Bypass content access control".' . "\r\n" . 'cintakita',
    '#access' => FALSE,
  );
  
  $form['#attached'] = array(
    'css' => array(
      drupal_get_path('module', 'access_query') . '/access_query.css',
    ),
    'js' => array(
      drupal_get_path('module', 'access_query') . '/access_query.js',
      // array(
        // 'type' => 'setting',
        // 'data' => array(
          // 'access_query_node' => array(
            // 'autoresize' => TRUE,
          // ),
        // ),
      // ),
    ),
  );
  // $debugname = 'form'; dpm($$debugname, '$' . $debugname);
  return system_settings_form($form);
}

function access_query_node_sources_validate(&$form, &$form_state) {
  // $debugname = 'form'; dpm($$debugname, '$' . $debugname);
  // $debugname = 'form_state'; dpm($$debugname, '$' . $debugname);
  // $form['access_query_node_custom']['#rows'] = 4;
  $message = AccessQueryNode::validateQueries($form_state['values']['access_query_node_custom']);
  if (!empty($message)) {
    // Repair value of #rows with current value.
    $current_value = $form['access_query_node_custom']['#value'];
    $form['access_query_node_custom']['#rows'] = count(preg_split('[\r\n]', $current_value));
    // Set error.
    form_set_error('access_query_node_custom', $message);
  }
}

function access_query_node_ui($form, &$form_state) {
  $form['element']['#markup'] = 'On Progress';
  return $form;
}

function access_query_node_above() {
  $queries = $query = array();  
  
  // Check of bypass node access.
  $result = db_query("SELECT rid FROM {role_permission} WHERE permission = :permission", array(':permission' => 'bypass node access'))->fetchAllAssoc('rid');
  $query['access'] = 'can';
  $query['access'] = 'can';
  
  if (!empty($result)) {
    if (isset($result[DRUPAL_AUTHENTICATED_RID]) && isset($result[DRUPAL_ANONYMOUS_RID])) {
      $query['scoupe'] = 'everyone';
    }
    else {
      
      
    }
    
    foreach ($result as $rid => $info) {
      
    }
  }
  $result = db_query("SELECT rid FROM {role_permission} WHERE permission = :permission", array(':permission' => 'access content'))->fetchAllAssoc('rid');
  // $debugname = 'result'; dpm($$debugname, '$' . $debugname);
  
  
}


function access_query_node_remove_rebuild_flag() {
  variable_set('access_query_node_rebuild', FALSE);
  
}