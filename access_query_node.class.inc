<?php

// label of content type can contains " (double quote) ' (single quote) and comma ,
// hook_module_implements_alter



class AccessQueryNode {

  public static $regex = 'role\s+(?<role>[[:alnum:]]+)(\s+whose\s+(?<spesific>.+))*\s+(?<access>can|cannot|can not|can\'t)\s+(?<op>create|edit|delete|view)\s+(?<scoupe>any|own)*\s*(?<type>[[:lower:]]+)*\s+content(\s+|$)(\s(?<extras>.*))*';
  
  /**
   * Fungsi untuk memvalidasi inputan user tentang query.
   */
  public static function validateQueries($string) {
    if (empty($string)) {
      return;
    }
    // split berdasarkan \r dan atau \n
    $lines = preg_split('[\r\n]', $string);
    foreach ($lines as $i => $line) {
      if ($message = self::validateQuery($line)) {
        $message = t('Error at line !order: %line. !message', array('!order' => $i + 1, '%line' => $line, '!message' => $message));
        return $message;
      }
    }
  }

  /**
   *
   Jika return NULL, maka baris yang divalidasi valid.
   Jika return string, maka string itu menjadi pesan error yang terjadi
   pada baris yang bersangkutan.
   Jika error pada satu baris terjadi, maka baris selanjutnya tidak diperiksa.


   *
   */
  public static function validateQuery($string) {
    $string = trim($string);
    if (empty($string)) {
      return;
    }
    $comment_sign = strpos($string, '#');
    if ($comment_sign === 0) {
      return;
    }
    // Convert to lower, to save in static variable.
    $query = strtolower($string);
    if (is_int($comment_sign)) {
      $query = rtrim(substr($query, 0, $comment_sign));
    }
    try {
      static $storage = array();
      if (in_array($query, $storage)) {
        $message = t('Duplicate entry.');
        throw new Exception($message);
      }

      // Start parsing query per line.
      $regex = self::$regex;
      preg_match('/' . $regex . '/i', $query, $matches);

      // For Developing.
      $debugname = 'matches'; dpm($$debugname, '$' . $debugname);

      // Validate each component of query.
      // Variable $matches is an array should have index:
      // role, spesific, access, op, scoupe, type (, and extras).
      if (empty($matches)) {
        $message = t('Unknown format.');
        throw new Exception($message);
      }
      // Validate role.
      if ($message = self::validateRole($matches['role'])) {
        throw new Exception($message);
      }
      // Validate spesific.
      if (!empty($matches['spesific']) && $message = self::validateSpesific($matches['spesific'])) {
        throw new Exception($message);
      }
      // Validate operator.
      if ($message = self::validateOperator($matches['op'], $matches['scoupe'])) {
        throw new Exception($message);
      }
      // Validate type.
      if ($message = self::validateType($matches['type'])) {
        throw new Exception($message);
      }
      if (!empty($matches['extras']) && $message = self::validateExtras($matches['extras'])) {
        throw new Exception($message);
      }

      // If everything OK, you must set info on static variable.
      // for check duplicate entry.
      $storage[] = $query;

    }
    catch (Exception $e) {
      return $e->getMessage();
    }
  }

  protected static function validateOperator($op, $scoupe) {
    if ($op == 'create' && !empty($scoupe)) {
      $message = t('Operator %op don\'t need scoupe (any or own).', array('%op' => $op));
      return $message;
    }
    if ($op != 'create' && empty($scoupe)) {
      $message = t('Operator %op need scoupe (any or own).', array('%op' => $op));
      return $message;
    }
  }
  
  protected static function validateRole($check) {
    static $storage = array();
    if (!array_key_exists($check, $storage)) {
      try {
        if (is_numeric($check)) {
          $list = user_roles();
          if (!isset($list[$check])) {
            $message = t('Unknown role ID: @rid.', array('@rid' => $check));
            throw new Exception($message);
          }
        }
        elseif (module_exists('role_export')) {
          $role_load =  db_select('role', 'r')
            ->fields('r')
            ->condition('machine_name', $check)
            ->execute()
            ->fetchObject();
          if (!$role_load) {
            $message = t('Unknown role machine_name: %check.', array('%check' => $check));
            throw new Exception($message);
          }
        }
        else {
          $message = t('Unknown Role: %check, you must set valid role ID or role machine_name (which provided by <a href=!url>Role Export</a> module).', array('%check' => $check, '!url' => 'http://drupal.org/project/role_export'));
          throw new Exception($message);
        }
        // Set as no message.
        $storage[$check] = NULL;
      }
      catch (Exception $e) {
        $storage[$check] = $e->getMessage();
      }
    }
    return $storage[$check];
  }

  // todo
  protected static function validateSpesific($check) {
    // return 'a';
  }
  // todo
  protected static function validateExtras($check) {
    // return 'a';
    // return 'a';
    // $node_permission = access_query_node_node_permissions();
    // if (!isset($node_permission[$check])) {
      // return t('Unknown permission.');
    // }
    // $debugname = 'node_permission'; dpm($$debugname, '$' . $debugname);
    // $debugname = 'check'; dpm($$debugname, '$' . $debugname);
  }


  protected static function validateType($check) {
    static $storage = array();
    if (!array_key_exists($check, $storage)) {
      $storage[$check] = node_type_get_type($check);
    }
    if (!$storage[$check]) {
      $message = t('Unknown Content Type : %type.', array('%type' => $check));
      return $message;
    }
  }

  /**
   * Mengecek node permission yang telah diset pada halaman admin permission,
   * kemudian membuat querynya. 
   * 
   * @return 
   *   Array berisi daftar query. Contoh: 
   *     array(
   *       'role 2 can edit own article content',
   *       'role 3 can create article content',
   *       'role 3 can create page content',
   *     )
   */
  public static function buildQueriesFromPermissionPage() {
    $role_names = user_roles();    
    $role_permissions = user_role_permissions($role_names);    
    $our_permission = access_query_node_node_permissions();    
    $storage = array();
    foreach ($role_permissions as $role => $permissions) {      
      $intersect = array();
      if (!empty($permissions)) {
        $intersect = array_intersect_key($permissions, $our_permission);
      }
      if (!empty($intersect)) {
        $intersect = array_keys($intersect);
        array_walk($intersect, function (&$v, $i, $role) {
          $v = 'role ' . $role . ' can ' . $v;          
        }, $role);
        $storage = array_merge($storage, $intersect);
      }
    }
    return $storage;
  }
  
  public static function buildQueriesFromString($string) {    
    $storage = array();
    $regex = self::$regex;    
    $lines = preg_split('[\r\n]', $string);
    foreach ($lines as $string) {
      $string = trim($string);      
      if (empty($string)) {
        continue;
      }
      $comment_sign = strpos($string, '#');
      if ($comment_sign === 0) {
        continue;
      }
      $query = strtolower($string);
      if (is_int($comment_sign)) {
        $query = rtrim(substr($query, 0, $comment_sign));
      }
      preg_match('/' . $regex . '/i', $query, $matches);      
      if (!empty($matches)) {
        $storage[] = $query;        
      }
    }
    return $storage;
  }
}


      // static $node_permission;
      // if (empty($node_permission)) {
        // $node_permission = implode('|', array_keys(access_query_node_node_permissions()));
      // }
      // $regex = 'role\s(?<role>[[:alnum:]]+)(\swhose\s(?<spesific>.+))*\s(?<access>can|cannot|can not|can\'t)\s(?<permission>' . $node_permission . ')(\s|$)(\s(?<extras>.*))*';
      // $regex = 'role\s*(?<role>[[:alnum:]]+)(\s*whose\s*(?<spesific>.+))*\s*(?<access>can|cannot|can not|can\'t)\s*(?<permission>' . $node_permission . ')(\s*(?<extras>.+)*|$)';
      // $regex = 'role\s+(?<role>[[:alnum:]]+)(\s+whose\s+(?<spesific>.+))*\s+(?<access>can|cannot|can not|can\'t)\s+(?<op>create|edit\s+(?<scoupe>any|own)|delete|view).*content';
      // $regex = 'role\s+(?<role>[[:alnum:]]+)(\s+whose\s+(?<spesific>.+))*\s+(?<access>can|cannot|can not|can\'t)\s+(?<op>create|edit|delete|view)\s+(?<scoupe>any|own)*(?<type>[[:lower:]])*\s+content';
